# SDCard Arduino library

SDCard is a slightly modified version of well known Arduino SD library.
The main difference is that SDCard don't uses malloc/free function, thus
the section of avrlibc, that manages heap, can be not linked to program.
On 8-bit AVRs this can reduce memory requirements.

To avoid conflicts with SD library all the datatypes is placed in SDCard namespace.

# SD Library for Arduino

The SD library allows for reading from and writing to SD cards.

For more information about this library please visit us at
[http://www.arduino.cc/en/Reference/SD](http://www.arduino.cc/en/Reference/SD)

## License

Copyright (C) 2009 by William Greiman
Copyright (c) 2010 SparkFun Electronics

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [<http://www.gnu.org/licenses/>](http://www.gnu.org/licenses/).
