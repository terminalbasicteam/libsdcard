/*
 * SDCard is a slightly modified version of standard Arduino SD library
 * Copyright (С) 2009 by William Greiman
 * Copyright (С) 2010 SparkFun Electronics
 * Copyright (C) 2017-1019 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * SD - a slightly more friendly wrapper for sdfatlib
 * This library aims to expose a subset of SD card functionality
 * in the form of a higher level "wrapper" object.
 */

#ifndef SDCARD_HPP
#define SDCARD_HPP

#include <Arduino.h>

#include <utility/sdfat.hpp>
#include <utility/sd2_file.hpp>
#include <utility/sd2_volume.hpp>

#include <stdint.h>

namespace SDCard
{

using namespace SD2;

/**
 * @brief File open mode
 */
enum class Mode : uint8_t
{
	NONE = 0,
	APPEND = O_APPEND,
	READ = O_READ,
	WRITE = O_WRITE,
	CREAT = O_CREAT,
	READ_ONLY = O_RDONLY
};

inline Mode operator |(Mode left, Mode right)
{
	return Mode(uint8_t(left) | uint8_t(right));
}

inline Mode operator &(Mode left, Mode right)
{
	return Mode(uint8_t(left) & uint8_t(right));
}

/**
 * @brief File object
 */
class File : public Stream
{
public:
	
	explicit File();
	/**
	 * @breif C-tor, wraps an underlying SdFile
	 * @param file SDFile to wrap
	 * @param name File name
	 */
	explicit File(const SdFile&, const char*);
	
	~File() = default;
	
	/**
	 * @brief Read bytes
         * 
         * Buffered read for more efficient, high speed reading
         * 
	 * @param buf buffer pointer
	 * @param nbyte buffer size
	 * @return bytes have been read
	 */
	int read(void*, uint16_t);
        
	bool seek(uint32_t);
        
	uint32_t position();
        
	uint32_t size();
        
	void close();
        
	operator bool();
        
	char *name();

	bool isDirectory();
	
	File openNextFile(Mode = Mode::READ_ONLY);
	
	void rewindDirectory();
	
private:
	// file name with DOS restrictions (8+3+dot+null)
	char m_name[13];
	// underlying file object
	SdFile m_file;
	bool m_initialized;
	
// Print interface
public:
	size_t write(uint8_t) override;
	size_t write(const uint8_t*, size_t) override;

	using Print::write;
// Stream interface
public:
	int read() override;
	int peek() override;
	int available() override;
	void flush() override;
};

/**
 * @brief SD card object
 */
class SDClass
{
public:
	/**
	 * @brief start library
	 * 
	 * This needs to be called to set up the connection to the SD card
	 * before other methods are used.
	 * 
	 * @param csPin CS pin
	 * @return success flag
	 */
	bool begin(uint8_t = SD_CHIP_SELECT_PIN);
	/**
	 * @brief overloaded version
	 * @param clock SPI clock to use
	 * @param csPin CS pin
	 * @return success flag
	 */
	bool begin(uint32_t clock, uint8_t csPin);

	/**
	 * @brief Open file or directory
	 * 
	 * Open the specified file/directory with the supplied mode (e.g. read or
	 * write, etc). Note that currently only one file can be open at a time.
	 * 
	 * @param name file or directory name
	 * @param mode open mode
	 * @return File object for interacting with the file.
	 */
	File open(const char*, Mode = Mode::READ);
	/**
	 * @brief overloaded version
	 */
	File open(const String &filename, Mode mode = Mode::READ)
	{
		return open(filename.c_str(), mode);
	}

	/**
	 * @brief Determine if the requested file path exists.
	 * @param path Path to test
	 * @return existance flag
	 */
	bool exists(const char*);
	/**
	 * @brief overloaded version
	 */
	bool exists(const String &filepath)
	{
		return exists(filepath.c_str());
	}

	/**
	 * @brief Create the requested directory heirarchy
	 * 
	 * If intermediate directories do not exist they will be created.
	 * 
	 * @param path
	 * @return flag of success
	 */
	bool mkdir(const char*);
	/**
	 * @brief overloaded version
	 */
	bool mkdir(const String &filepath)
	{
		return mkdir(filepath.c_str());
	}

	/**
	 * @brief Delete the file.
	 * @param path
	 */
	bool remove(const char*);
	/**
	 * @brief overloaded version
	 */
	bool remove(const String &filepath)
	{
		return remove(filepath.c_str());
	}

	bool rmdir(const char*);
	/**
	 * @brief overloaded version
	 */
	bool rmdir(const String &filepath)
	{
		return rmdir(filepath.c_str());
	}

private:
	// my quick&dirty iterator, should be replaced
	SdFile getParentDir(const char *filepath, int *indx);
	// These are required for initialisation and use of sdfatlib
	Sd2Card card;
	SdVolume volume;
	SdFile root;

	// This is used to determine the mode used to open a file
	// it's here because it's the easiest place to pass the 
	// information through the directory walking function. But
	// it's probably not the best place for it.
	// It shouldn't be set directly--it is set via the parameters to `open`.
	int fileOpenMode;

	friend class File;
	friend bool callback_openPath(SdFile&, const char *, bool, void *);
};

extern SDClass SDFS;

} // namespace SDCard

#endif // SDCARD_HPP
