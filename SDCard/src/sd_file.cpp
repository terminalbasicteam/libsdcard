/*
 * SDCard is a slightly modified version of standard Arduino SD library
 * Copyright (С) 2009 by William Greiman
 * Copyright (С) 2010 SparkFun Electronics
 * Copyright (C) 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * SD - a slightly more friendly wrapper for sdfatlib
 * This library aims to expose a subset of SD card functionality
 * in the form of a higher level "wrapper" object.
 */

#include "sd.hpp"

namespace SDCard
{

#define SD_DEBUG 0

#if SD_DEBUG
// for debugging file open/close leaks
static uint8_t nfilecount = 0;
#endif

File::File(const SdFile &f, const char *n) :
    m_initialized(true)
{
	memcpy(&m_file, &f, sizeof (SdFile));
	strncpy(m_name, n, 12);
	// Prrotect code, if ther isn't 0 byte among 12 first bytes of n
	m_name[12] = 0;

#if SD_DEBUG
	++nfilecount;
	Serial.print("Created \"");
	Serial.print(n);
	Serial.print("\": ");
	Serial.println(nfilecount, DEC);
#endif
}

File::File() :
    m_initialized(0)
{
	m_name[0] = 0;
#if SD_DEBUG
	Serial.print("Created empty file object");
#endif
}

// returns a pointer to the file name
char *
File::name()
{
	return m_name;
}

// a directory is a special type of file
bool
File::isDirectory(void)
{
	return m_initialized && m_file.isDir();
}

size_t
File::write(uint8_t val)
{
	return write(&val, 1);
}

size_t
File::write(const uint8_t *buf, size_t size)
{
	size_t t;
	if (!m_initialized) {
		setWriteError();
		return 0;
	}
	m_file.clearWriteError();
	t = m_file.write(buf, size);
	if (m_file.getWriteError()) {
		setWriteError();
		return 0;
	}
	return t;
}

int
File::peek()
{
	if (m_initialized) {
		int c = m_file.read();
		if (c != -1)
			m_file.seek(-1, SdFile::S_CUR);
		return c;
	} else
		return 0;
}

int
File::read()
{
	if (m_initialized)
		return m_file.read();
	else
		return -1;
}

int
File::read(void *buf, uint16_t nbyte)
{
	if (m_initialized)
		return m_file.read(buf, nbyte);
	else
		return 0;
}

int
File::available()
{
	if (m_initialized) {
		uint32_t n = size() - position();
		return n > 0X7FFF ? 0X7FFF : n;
	} else
		return 0;
}

void
File::flush()
{
	if (m_initialized)
		m_file.sync();
}

bool
File::seek(uint32_t pos)
{
	if (m_initialized)
		return m_file.seek(pos);
	else
		return false;
}

uint32_t
File::position()
{
	if (m_initialized)
		return m_file.curPosition();
	else
		return 0;
}

uint32_t
File::size()
{
	if (m_initialized)
		return m_file.fileSize();
	else
		return 0;
}

void
File::close()
{
	if (m_initialized) {
		m_file.close();
		m_initialized = false;

#if SD_DEBUG
		--nfilecount;
		Serial.print("Deleted ");
		Serial.println(nfilecount, DEC);
#endif
	}
}

File::operator bool()
{
	if (m_initialized)
		return m_file.isOpen();
	else
		return false;
}

} // namespace SDCard
