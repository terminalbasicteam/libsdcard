/* Arduino SdFat Library
 * Copyright (C) 2009 by William Greiman
 * Copyright (C) 2017-1019 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This file is part of the Arduino SdFat Library
 *
 * This Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Arduino SdFat Library.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef SD2_FILE_HPP
#define SD2_FILE_HPP

#include <Print.h>
#include <stdint.h>

namespace SD2
{

/**
 * \class SdFile
 * \brief Access FAT16 and FAT32 files on SD and SDHC cards.
 */
class SdFile : public Print
{
public:

	/** Create an instance of SdFile. */
	SdFile();
	/**
	 * writeError is set to true if an error occurs during a write().
	 * Set writeError to false before calling print() and/or write() and check
	 * for true after calls to print() and/or write().
	 */
	//bool writeError;

	/**
	 * Cancel unbuffered reads for this file.
	 * See setUnbufferedRead()
	 */
	void clearUnbufferedRead();
	
	bool close();
	
	bool contiguousRange(uint32_t* bgnBlock, uint32_t* endBlock);
	
	bool createContiguous(SdFile* dirFile, const char* fileName,
				uint32_t size);

	/** \return The current cluster number for a file or directory. */
	uint32_t curCluster() const { return curCluster_; }

	/** \return The current position for a file or directory. */
	uint32_t curPosition() const { return curPosition_; }

	/**
	 * Set the date/time callback function
	 *
	 * \param[in] dateTime The user's call back function.  The callback
	 * function is of the form:
	 *
	 * \code
	 * void dateTime(uint16_t* date, uint16_t* time) {
	 *   uint16_t year;
	 *   uint8_t month, day, hour, minute, second;
	 *
	 *   // User gets date and time from GPS or real-time clock here
	 *
	 *   // return date using FAT_DATE macro to format fields
	 *   *date = FAT_DATE(year, month, day);
	 *
	 *   // return time using FAT_TIME macro to format fields
	 *   *time = FAT_TIME(hour, minute, second);
	 * }
	 * \endcode
	 *
	 * Sets the function that is called when a file is created or when
	 * a file's directory entry is modified by sync(). All timestamps,
	 * access, creation, and modify, are set when a file is created.
	 * sync() maintains the last access date and last modify date/time.
	 *
	 * See the timestamp() function.
	 */
	static void dateTimeCallback(void (*dateTime)(uint16_t* date, uint16_t* time));

	/**
	 * Cancel the date/time callback function.
	 */
	static void dateTimeCallbackCancel();

	/** \return Address of the block that contains this file's directory. */
	uint32_t dirBlock() const { return dirBlock_; }
	
	bool dirEntry(dir_t* dir);

	/** \return Index of this file's directory in the block dirBlock. */
	uint8_t	dirIndex() const { return dirIndex_; }
	
	static void dirName(const dir_t& dir, char* name);

	/** \return The total number of bytes in a file or directory. */
	uint32_t fileSize() const { return fileSize_; }

	/** \return The first cluster number for a file or directory. */
	uint32_t firstCluster() const { return firstCluster_; }

	/** \return True if this is a SdFile for a directory else false. */
	bool isDir() const;

	/** \return True if this is a SdFile for a file else false. */
	bool isFile() const;

	/** \return True if this is a SdFile for an open file/directory else false. */
	bool isOpen() const;

	/** \return True if this is a SdFile for a subdirectory else false. */
	bool isSubDir() const;

	/** \return True if this is a SdFile for the root directory. */
	bool isRoot() const;
	
	void ls(Print&, uint8_t flags = 0, uint8_t indent = 0);
	
	bool makeDir(SdFile* dir, const char* dirName);
	
	bool open(SdFile* dirFile, uint16_t index, uint8_t oflag);
	
	bool open(SdFile* dirFile, const char* fileName, uint8_t oflag);

	bool openRoot(SdVolume* vol);

	static void printDirName(Print&, const dir_t& dir, uint8_t width);
	
	static void printFatDate(Print&, uint16_t fatDate);
	
	static void printFatTime(Print&, uint16_t fatTime);
	
	static void printTwoDigits(Print&, uint8_t v);

	/**
	 * Read the next byte from a file.
	 *
	 * \return For success read returns the next byte in the file as an int.
	 * If an error occurs or end of file is reached -1 is returned.
	 */
	int16_t	read();
	
	int16_t read(void* buf, uint16_t nbyte);
	
	static bool remove(SdFile* dirFile, const char* fileName);
	
	bool remove();

	/** Set the file's current position to zero. */
	void rewind();
	
	bool rmDir();
	
	bool rmRfStar();

	enum Whence_t : uint8_t
	{
		S_SET, S_CUR, S_END
	};
	
	bool seek(uint32_t, Whence_t = S_SET);

	/**
	 * Use unbuffered reads to access this file.  Used with Wave
	 * Shield ISR.  Used with Sd2Card::partialBlockRead() in WaveRP.
	 *
	 * Not recommended for normal applications.
	 */
	void setUnbufferedRead();
	
	bool timestamp(uint8_t flag, uint16_t year, uint8_t month, uint8_t day,
			uint8_t hour, uint8_t minute, uint8_t second);
	
	bool sync();

	/** Type of this SdFile.  You should use isFile() or isDir() instead of type()
	 * if possible.
	 *
	 * \return The file or directory type.
	 */
	uint8_t	type() const { return type_; }
	
	bool truncate(uint32_t size);

	/** \return Unbuffered read flag. */
	uint8_t	unbufferedRead() const;

	/**
	 * @return SdVolume that contains this file.
	 */
	SdVolume* volume() const { return vol_; }

	size_t write(uint8_t b);
	size_t write(const void* buf, uint16_t nbyte);
	size_t write(const char* str);
#ifdef __AVR__
	void write_P(PGM_P str);
	void writeln_P(PGM_P str);
#endif
	//------------------------------------------------------------------------------
#if ALLOW_DEPRECATED_FUNCTIONS
	// Deprecated functions  - suppress cpplint warnings with NOLINT comment

	/** \deprecated Use:
	 * uint8_t SdFile::contiguousRange(uint32_t* bgnBlock, uint32_t* endBlock);
	 */
	bool contiguousRange(uint32_t& bgnBlock, uint32_t& endBlock);

	/** \deprecated Use:
	 * uint8_t SdFile::createContiguous(SdFile* dirFile,
	 *   const char* fileName, uint32_t size)
	 */
	bool createContiguous(SdFile& dirFile, // NOLINT
			const char* fileName, uint32_t size);

	/**
	 * \deprecated Use:
	 * static void SdFile::dateTimeCallback(
	 *   void (*dateTime)(uint16_t* date, uint16_t* time));
	 */
	static void dateTimeCallback(void (*dateTime)(uint16_t& date, uint16_t& time));

	/** \deprecated Use: uint8_t SdFile::dirEntry(dir_t* dir); */
	bool dirEntry(dir_t& dir); // NOLINT

	/** \deprecated Use:
	 * uint8_t SdFile::makeDir(SdFile* dir, const char* dirName);
	 */
	bool makeDir(SdFile& dir, const char* dirName);

	/** \deprecated Use:
	 * uint8_t SdFile::open(SdFile* dirFile, const char* fileName, uint8_t oflag);
	 */
	bool open(SdFile& dirFile, const char* fileName, uint8_t oflag);

	/** \deprecated  Do not use in new apps */
	bool open(SdFile& dirFile, const char* fileName);

	/** \deprecated Use:
	 * uint8_t SdFile::open(SdFile* dirFile, uint16_t index, uint8_t oflag);
	 */
	bool open(SdFile& dirFile, uint16_t index, uint8_t oflag);

	/** \deprecated Use: uint8_t SdFile::openRoot(SdVolume* vol); */
	bool openRoot(SdVolume& vol); // NOLINT

	int8_t readDir(dir_t& dir); // NOLINT

	/** \deprecated Use:
	 * static uint8_t SdFile::remove(SdFile* dirFile, const char* fileName);
	 */
	static uint8_t remove(SdFile& dirFile, const char* fileName);
	//------------------------------------------------------------------------------
	// rest are private
private:
	static void (*oldDateTime_)(uint16_t& date, uint16_t& time); // NOLINT

	static void oldToNew(uint16_t* date, uint16_t* time);
#endif  // ALLOW_DEPRECATED_FUNCTIONS
private:

	bool _seekSet(uint32_t pos);
	
	bool addCluster();
	
	bool addDirCluster();
	
	dir_t* cacheDirEntry(uint8_t action);
	
	static void (*dateTime_)(uint16_t* date, uint16_t* time);
	
	static bool make83Name(const char* str, uint8_t* name);
	
	bool openCachedEntry(uint8_t cacheIndex, uint8_t oflags);
	
	dir_t* readDirCache();

	// bits defined in flags_
	// should be 0XF
	static uint8_t const F_OFLAG = (O_ACCMODE | O_APPEND | O_SYNC);
	// available bits
	static uint8_t const F_UNUSED = 0X30;
	// use unbuffered SD read
	static uint8_t const F_FILE_UNBUFFERED_READ = 0X40;
	// sync of directory entry required
	static uint8_t const F_FILE_DIR_DIRTY = 0X80;

	// make sure F_OFLAG is ok
#if ((F_UNUSED | F_FILE_UNBUFFERED_READ | F_FILE_DIR_DIRTY) & F_OFLAG)
#error flags_ bits conflict
#endif  // flags_ bits

	// private data
	uint8_t flags_; // See above for definition of flags_ bits
	uint8_t type_; // type of file see above for values
	uint32_t curCluster_; // cluster for current file position
	uint32_t curPosition_; // current file position in bytes from beginning
	uint32_t dirBlock_; // SD block that contains directory entry for file
	uint8_t dirIndex_; // index of entry in dirBlock 0 <= dirIndex_ <= 0XF
	uint32_t fileSize_; // file size in bytes
	uint32_t firstCluster_; // first cluster of file
	SdVolume* vol_; // volume where file is located
};

} // namespace SD2

#endif /* SD2_FILE_HPP */
