/* Arduino SdFat Library
 * Copyright (C) 2009 by William Greiman
 * Copyright (C) 2017-1019 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This file is part of the Arduino SdFat Library
 *
 * This Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Arduino SdFat Library.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef SDFAT_HPP
#define SDFAT_HPP

/**
 * \file
 * SdFile and SdVolume classes
 */
#if defined (__AVR__) || defined (__CPU_ARC__) 
#include <avr/pgmspace.h>
#endif
#include "sd2_card.hpp"
#include "fatstructs.hpp"

#include <Print.h>

namespace SD2
{

//------------------------------------------------------------------------------
/**
 * Allow use of deprecated functions if non-zero
 */
#define ALLOW_DEPRECATED_FUNCTIONS 1
//------------------------------------------------------------------------------
// forward declaration since SdVolume is used in SdFile
class SdVolume;
//==============================================================================
// SdFile class

// flags for ls()
/** ls() flag to print modify date */
uint8_t const LS_DATE = 1;
/** ls() flag to print file size */
uint8_t const LS_SIZE = 2;
/** ls() flag for recursive list of subdirectories */
uint8_t const LS_R = 4;

// use the gnu style oflag in open()
/** open() oflag for reading */
uint8_t const O_READ = 0X01;
/** open() oflag - same as O_READ */
uint8_t const O_RDONLY = O_READ;
/** open() oflag for write */
uint8_t const O_WRITE = 0X02;
/** open() oflag - same as O_WRITE */
uint8_t const O_WRONLY = O_WRITE;
/** open() oflag for reading and writing */
uint8_t const O_RDWR = (O_READ | O_WRITE);
/** open() oflag mask for access modes */
uint8_t const O_ACCMODE = (O_READ | O_WRITE);
/** The file offset shall be set to the end of the file prior to each write. */
uint8_t const O_APPEND = 0X04;
/** synchronous writes - call sync() after each write */
uint8_t const O_SYNC = 0X08;
/** create the file if nonexistent */
uint8_t const O_CREAT = 0X10;
/** If O_CREAT and O_EXCL are set, open() shall fail if the file exists */
uint8_t const O_EXCL = 0X20;
/** truncate the file to zero length */
uint8_t const O_TRUNC = 0X40;

// flags for timestamp
/** set the file's last access date */
uint8_t const T_ACCESS = 1;
/** set the file's creation date and time */
uint8_t const T_CREATE = 2;
/** Set the file's write date and time */
uint8_t const T_WRITE = 4;
// values for type_
/** This SdFile has not been opened. */
uint8_t const FAT_FILE_TYPE_CLOSED = 0;
/** SdFile for a file */
uint8_t const FAT_FILE_TYPE_NORMAL = 1;
/** SdFile for a FAT16 root directory */
uint8_t const FAT_FILE_TYPE_ROOT16 = 2;
/** SdFile for a FAT32 root directory */
uint8_t const FAT_FILE_TYPE_ROOT32 = 3;
/** SdFile for a subdirectory */
uint8_t const FAT_FILE_TYPE_SUBDIR = 4;
/** Test value for directory type */
uint8_t const FAT_FILE_TYPE_MIN_DIR = FAT_FILE_TYPE_ROOT16;

/** date field for FAT directory entry */
static inline uint16_t
FAT_DATE(uint16_t year, uint8_t month, uint8_t day)
{
	return (year - 1980) << 9 | month << 5 | day;
}

/** year part of FAT directory date field */
static inline uint16_t
FAT_YEAR(uint16_t fatDate)
{
	return 1980 + (fatDate >> 9);
}

/** month part of FAT directory date field */
static inline uint8_t
FAT_MONTH(uint16_t fatDate)
{
	return (fatDate >> 5) & 0XF;
}

/** day part of FAT directory date field */
static inline uint8_t
FAT_DAY(uint16_t fatDate)
{
	return fatDate & 0X1F;
}

/** time field for FAT directory entry */
static inline uint16_t
FAT_TIME(uint8_t hour, uint8_t minute, uint8_t second)
{
	return hour << 11 | minute << 5 | second >> 1;
}

/** hour part of FAT directory time field */
static inline uint8_t
FAT_HOUR(uint16_t fatTime)
{
	return fatTime >> 11;
}

/** minute part of FAT directory time field */
static inline uint8_t
FAT_MINUTE(uint16_t fatTime)
{
	return (fatTime >> 5) & 0X3F;
}

/** second part of FAT directory time field */
static inline uint8_t
FAT_SECOND(uint16_t fatTime)
{
	return 2 * (fatTime & 0X1F);
}
/** Default date for file timestamps is 1 Jan 2000 */
uint16_t const FAT_DEFAULT_DATE = ((2000 - 1980) << 9) | (1 << 5) | 1;
/** Default time for file timestamp is 1 am */
uint16_t const FAT_DEFAULT_TIME = (1 << 11);
//------------------------------------------------------------------------------

//==============================================================================
// SdVolume class

/**
 * \brief Cache for an SD data block
 */
union cache_t
{
	/** Used to access cached file data blocks. */
	uint8_t data[512];
	/** Used to access cached FAT16 entries. */
	uint16_t fat16[256];
	/** Used to access cached FAT32 entries. */
	uint32_t fat32[128];
	/** Used to access cached directory entries. */
	dir_t dir[16];
	/** Used to access a cached MasterBoot Record. */
	mbr_t mbr;
	/** Used to access to a cached FAT boot sector. */
	fbs_t fbs;
};

} // namespace SD2

#endif  // SdFat_h
